package com.example.annonce;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textViewMess;
    Button boutonAffiche;
    Personne P;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewMess = findViewById(R.id.textViewAff);
        boutonAffiche = findViewById(R.id.boutonAff);

        P = new Personne("Doe", "John", true, "2000-01-01", "Ingénieur", 175.0f, "Marron", 70.0f, "Française");
        P.setTaille(177.0f);
        // on va ecouter le bouton
        boutonAffiche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                P.augmenterTaille(0.10f);
                String textAff = String.format("La taille de %s : %.2f", P.prenom, P.getTaille());
                textViewMess.setText(textAff);
                Log.d("TEST_ON_CLICK", "onClick: "+textAff);
            }
        });
    }
}