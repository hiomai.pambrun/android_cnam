package com.example.annonce;

public class Personne {
    String nom;
    String prenom;
    Boolean sexe;
    String dateDeNaissance;
    String profession;
    private float taille;
    String couleurDesYeux;
    float poids;
    String nationalite;

    public Personne() {
    }

    public Personne(String nom, String prenom, Boolean sexe, String dateDeNaissance, String profession, float taille, String couleurDesYeux, float poids, String nationalite) {
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.dateDeNaissance = dateDeNaissance;
        this.profession = profession;
        this.taille = taille;
        this.couleurDesYeux = couleurDesYeux;
        this.poids = poids;
        this.nationalite = nationalite;
    }

    public void setTaille(float taille) {
        this.taille = taille;
    }

    public float getTaille(){
        return this.taille;
    }

    public void augmenterTaille(float augmenterTaille) {
        this.taille = this.taille+augmenterTaille;
    }
}
